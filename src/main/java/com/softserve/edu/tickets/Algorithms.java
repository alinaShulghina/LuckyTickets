package com.softserve.edu.tickets;

/**
 * Created by alin- on 17.12.2017.
 */
public enum Algorithms {

    MOSCOW(new MoscowAlgorithm()), PITER(new PiterAlgorithm());

    private LuckyTicketAlgorithm algorithm;

    Algorithms(LuckyTicketAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public LuckyTicketAlgorithm getAlgorithm() {
        return algorithm;
    }
}
