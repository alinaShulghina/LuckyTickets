package com.softserve.edu.tickets;

/**
 * Created by alin- on 10.12.2017.
 */
public class PiterAlgorithm implements LuckyTicketAlgorithm {

    private int totalLuckyTickets;

    public PiterAlgorithm() {
        this.totalLuckyTickets = countLuckyTickets();
    }

    @Override
    public int getTotalLuckyTickets() {
        return totalLuckyTickets;
    }

    @Override
    public String getAlgorithmName() {
        return "PiterAlgorithm";
    }

    private int countLuckyTickets() {
        int count = 1;
        for (int i = 101; i < NUMBERS; i++) {
            if (isTicketLucky(i)) {
                count++;
            }
        }
        return count;
    }

    public boolean isTicketLucky(int ticket) {
        int oddNumbers = 0;
        int evenNumbers = 0;
        for (int i = 0; i < 6; i++) {
            int lastNumber = ticket % 10;
            if (lastNumber % 2 == 0) {
                evenNumbers += lastNumber;
            } else {
                oddNumbers += lastNumber;
            }
            ticket = (ticket - lastNumber) / 10;
        }
        return (oddNumbers == evenNumbers);
    }


    @Override
    public String toString() {
        return "PiterAlgorithm{" +
                "totalLuckyTickets=" + totalLuckyTickets +
                '}';
    }
}
