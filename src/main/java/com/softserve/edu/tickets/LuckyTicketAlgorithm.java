package com.softserve.edu.tickets;

/**
 * Created by alin- on 10.12.2017.
 */
public interface LuckyTicketAlgorithm {
    int NUMBERS = 1000000; //total amount of numbers from 000000 to 999999

    String getAlgorithmName();

    int getTotalLuckyTickets();
}
