package com.softserve.edu.tickets;

/**
 * Created by alin- on 10.12.2017.
 */
public class MoscowAlgorithm implements LuckyTicketAlgorithm {

    private int totalLuckyTickets;

    public MoscowAlgorithm() {
        this.totalLuckyTickets = countLuckyTickets();
    }

    @Override
    public int getTotalLuckyTickets() {
        return totalLuckyTickets;
    }

    @Override
    public String getAlgorithmName() {
        return "MoscowAlgorithm";
    }

    private int countLuckyTickets() {
        int maxSumm = 9 + 9 + 9;
        int[] summs = new int[maxSumm + 1];
        int happyTickets = 0;
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                for (int k = 0; k < 10; ++k) {
                    ++summs[i + j + k];
                }
            }
        }
        for (int i = 0; i <= maxSumm; ++i) {
            happyTickets += summs[i] * summs[i];
        }
        return happyTickets;
    }


    @Override
    public String toString() {
        return "MoscowAlgorithm{" +
                "totalLuckyTickets=" + totalLuckyTickets +
                '}';
    }
}
