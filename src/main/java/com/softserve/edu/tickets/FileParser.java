package com.softserve.edu.tickets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Created by alin- on 14.12.2017.
 */
public class FileParser {
    /*
        Fetch a key word from file
     */
    public static String getWordFromFile(String filePath) {
        String word = null;
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            word = stream.findFirst().get();
        } catch (IOException e) {
            System.out.println("Error reading file!");
        }
        return word;
    }
}
