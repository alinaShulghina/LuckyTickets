package com.softserve.edu.tickets;

import java.util.Scanner;

/**
 * Created by alin- on 10.12.2017.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String filePath = scanner.nextLine();
        String keyWord = FileParser.getWordFromFile(filePath);
        try {
            Algorithms algorithmsValue = Algorithms.valueOf(keyWord.toUpperCase());
            LuckyTicketAlgorithm algorithm = algorithmsValue.getAlgorithm();
            System.out.println(algorithm.getAlgorithmName() + " has " + algorithm.getTotalLuckyTickets() + " lucky tickets.");
        } catch (IllegalArgumentException e) {
            System.out.println("Incorrect algorithm name!");
        }
    }
}
