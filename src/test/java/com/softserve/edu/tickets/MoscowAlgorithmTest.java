package com.softserve.edu.tickets;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alin- on 15.12.2017.
 */
public class MoscowAlgorithmTest {

    private MoscowAlgorithm algorithm = new MoscowAlgorithm();

    @Test
    public void getTotalLuckyTicketsTest(){
        int totalLuckyTickets = 55252;

        assertEquals(totalLuckyTickets,algorithm.getTotalLuckyTickets());
    }

}